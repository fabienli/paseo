package ca.chancehorizon.paseo



import android.content.*
import android.database.sqlite.SQLiteException
import android.os.Bundle
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.AttrRes
import androidx.annotation.ColorInt
import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlinx.android.synthetic.main.fragment_dashboard.*
import kotlinx.android.synthetic.main.fragment_dashboard.view.*
import kotlinx.android.synthetic.main.fragment_dashboard.view.legendLayout
import java.text.NumberFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.max
import kotlin.math.min


class DashboardFragment : androidx.fragment.app.Fragment() {

    var running = false
    var startSteps = 0
    var currentSteps = 0
    var endSteps = 0

    // default values for target steps (overridden later from shared preferences)
    var targetSteps = 10000
    var targetWeekSteps = targetSteps * 7
    var targetMonthSteps = targetSteps * 30
    var targetYearSteps = targetSteps * 365

    var lastStepDate = 0

    var receiver: BroadcastReceiver? = null

    lateinit var paseoDBHelper : PaseoDBHelper



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // point to the Paseo database that stores all the daily steps data
        paseoDBHelper = PaseoDBHelper(requireActivity())

        // get the date of the last record from the steps table in the database
        lastStepDate = paseoDBHelper.readLastStepsDate()

        // get the start steps of the last record from the steps table in the database
        startSteps = paseoDBHelper.readLastStartSteps()

        startSteps = startSteps + 0

        updateDashboard()

        configureReceiver()

        running = true

    }



    // set up receiving messages from the step counter service
    private fun configureReceiver()
    {
        val filter = IntentFilter()
        filter.addAction("ca.chancehorizon.paseo.action")
        filter.addAction("android.intent.action.ACTION_POWER_DISCONNECTED")

        receiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                updateDashboard()
            }
        }

        context?.registerReceiver(receiver, filter)
    }



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        // Inflate the layout for this fragment
        val view : View = inflater.inflate(R.layout.fragment_dashboard, container, false)

        // respond to the user tapping on the dashboard legend
        //  (essentially making the legend into a button)
        view.legendLayout.setOnClickListener { view ->
            // show the dashboard legend detailed descriptions dialog sheet
            val bottomSheet = DashboardLegendFragment()
            bottomSheet.show(childFragmentManager, "BottomSheet")
        }

        // get the application settings (save messages etc)
        val paseoPrefs = context?.getSharedPreferences("ca.chancehorizon.paseo_preferences", 0)
        targetSteps = paseoPrefs!!.getFloat("prefDailyStepsTarget", 10000F).toInt()
        targetWeekSteps = targetSteps * 7
        targetMonthSteps = targetSteps * 30
        targetYearSteps = targetSteps * 365

        // respond to the user tapping on the day steps progress circles
        //  (essentially making the day steps progress circles a button)
        view.dayCard.setOnClickListener { view ->

            val theSteps = paseoDBHelper.getDaysSteps(SimpleDateFormat("yyyyMMdd").format(Date()).toInt())
            val onTarget = paseoDBHelper.getOnTarget("days", targetSteps)
            val averageSteps = paseoDBHelper.getAverageSteps("days", SimpleDateFormat("yyyyMMdd").format(Date()).toInt())
            val maxSteps = paseoDBHelper.getMaxSteps("days", SimpleDateFormat("yyyyMMdd").format(Date()).toInt())
            val minSteps = paseoDBHelper.getMinSteps("days", SimpleDateFormat("yyyyMMdd").format(Date()).toInt())
            val theHour = SimpleDateFormat("HH").format(Date()).toInt()

            // fill in all the step details in the details bottom sheet
            val bottomSheet = DashboardStepsDetailsFragment()

            bottomSheet.timeUnit = "day"

            bottomSheet.timeUnitSummaryTitle = "Summary for " + SimpleDateFormat("MMM dd, yyyy").format(Date())
            bottomSheet.steps = theSteps
            bottomSheet.expectedSteps = targetSteps * max(theHour, 1) / 24
            bottomSheet.projectedSteps = theSteps * 24 / max(theHour,1)
            bottomSheet.targetSteps = targetSteps

            bottomSheet.timesOnTarget = onTarget
            bottomSheet.averageSteps = averageSteps
            bottomSheet.maximumSteps = maxSteps
            bottomSheet.minimumSteps = minSteps

            bottomSheet.show(childFragmentManager, "BottomSheet")
        }

        // respond to the user tapping on the week steps progress circles
        //  (essentially making the week steps progress circles a button)
        view.weekCard.setOnClickListener { view ->

            val theSteps = paseoDBHelper.getSteps("weeks", SimpleDateFormat("yyyyMMdd").format(Date()).toInt())
            val onTarget = paseoDBHelper.getOnTarget("weeks", targetWeekSteps)
            val maxSteps = paseoDBHelper.getMaxSteps("weeks", SimpleDateFormat("yyyyMMdd").format(Date()).toInt())
            val minSteps = paseoDBHelper.getMinSteps("weeks", SimpleDateFormat("yyyyMMdd").format(Date()).toInt())
            val averageSteps = paseoDBHelper.getAverageSteps("weeks", SimpleDateFormat("yyyyMMdd").format(Date()).toInt())
            val theDayOfWeek = SimpleDateFormat("u").format(Date()).toInt()

            // show the dashboard steps detailed descriptions dialog sheet
            val bottomSheet = DashboardStepsDetailsFragment()
            bottomSheet.timeUnit = "week"

            bottomSheet.timeUnitSummaryTitle = "Summary for Week of " + SimpleDateFormat("MMM dd, yyyy").format(Date())
            bottomSheet.steps = theSteps
            bottomSheet.expectedSteps = targetWeekSteps * theDayOfWeek / 7
            bottomSheet.projectedSteps = theSteps * 7 / theDayOfWeek
            bottomSheet.targetSteps = targetWeekSteps

            bottomSheet.timesOnTarget = onTarget
            bottomSheet.averageSteps = averageSteps
            bottomSheet.maximumSteps = maxSteps
            bottomSheet.minimumSteps = minSteps

            bottomSheet.show(childFragmentManager, "BottomSheet")
        }

        // respond to the user tapping on the month steps progress circles
        //  (essentially making the month steps progress circles a button)
        view.monthCard.setOnClickListener { view ->

            val theSteps = paseoDBHelper.getSteps("months", SimpleDateFormat("yyyyMMdd").format(Date()).toInt())
            val onTarget = paseoDBHelper.getOnTarget("months", targetMonthSteps)
            val maxSteps = paseoDBHelper.getMaxSteps("months", SimpleDateFormat("yyyyMMdd").format(Date()).toInt())
            val minSteps = paseoDBHelper.getMinSteps("months", SimpleDateFormat("yyyyMMdd").format(Date()).toInt())
            val averageSteps = paseoDBHelper.getAverageSteps("months", SimpleDateFormat("yyyyMMdd").format(Date()).toInt())
            val theDayofMonth = SimpleDateFormat("d").format(Date()).toInt()
            val calendar = Calendar.getInstance()
            calendar.time = Date()
            val daysInMonth = calendar.getActualMaximum(Calendar.DAY_OF_MONTH) // 28

            // show the dashboard steps detailed descriptions dialog sheet
            val bottomSheet = DashboardStepsDetailsFragment()
            bottomSheet.timeUnit = "month"

            bottomSheet.timeUnitSummaryTitle = "Summary for " + SimpleDateFormat("MMMM").format(Date())
            bottomSheet.steps = theSteps
            bottomSheet.expectedSteps = targetMonthSteps * theDayofMonth / daysInMonth
            bottomSheet.projectedSteps = theSteps * daysInMonth / theDayofMonth
            bottomSheet.targetSteps = targetMonthSteps

            bottomSheet.timesOnTarget = onTarget
            bottomSheet.averageSteps = averageSteps
            bottomSheet.maximumSteps = maxSteps
            bottomSheet.minimumSteps = minSteps

            bottomSheet.show(childFragmentManager!!, "BottomSheet")
        }

        // respond to the user tapping on the year steps progress circles
        //  (essentially making the year steps progress circles a button)
        view.yearCard.setOnClickListener { view ->

            val theSteps = paseoDBHelper.getSteps("years", SimpleDateFormat("yyyyMMdd").format(Date()).toInt())
            val onTarget = paseoDBHelper.getOnTarget("years", targetYearSteps)
            val maxSteps = paseoDBHelper.getMaxSteps("years", SimpleDateFormat("yyyyMMdd").format(Date()).toInt())
            val minSteps = paseoDBHelper.getMinSteps("years", SimpleDateFormat("yyyyMMdd").format(Date()).toInt())
            val averageSteps = paseoDBHelper.getAverageSteps("years", SimpleDateFormat("yyyyMMdd").format(Date()).toInt())
            val calendar = Calendar.getInstance()
            calendar.time = Date()
            val daysInYear = calendar.getActualMaximum(Calendar.DAY_OF_YEAR)
            val theDayOfYear = calendar.get(Calendar.DAY_OF_YEAR) // the day of the year in numerical format

            // show the dashboard steps detailed descriptions dialog sheet
            val bottomSheet = DashboardStepsDetailsFragment()
            bottomSheet.timeUnit = "year"

            bottomSheet.timeUnitSummaryTitle = "Summary for " + SimpleDateFormat("YYYY").format(Date())
            bottomSheet.steps = theSteps
            bottomSheet.expectedSteps = targetYearSteps * theDayOfYear / daysInYear
            bottomSheet.projectedSteps = theSteps * daysInYear / theDayOfYear
            bottomSheet.targetSteps = targetYearSteps

            bottomSheet.timesOnTarget = onTarget
            bottomSheet.averageSteps = averageSteps
            bottomSheet.maximumSteps = maxSteps
            bottomSheet.minimumSteps = minSteps

            // use the main color from the current theme for the bars in the chart
            val theColor = context!!.getThemeColor(R.attr.background)

            bottomSheet.show(childFragmentManager!!, "BottomSheet")
        }

        return view
    }



    // re-update the screen when the user returns to it from another screen
    override fun onResume() {
        super.onResume()

        // get the application settings (save messages etc)
//        val paseoPrefs = context?.getSharedPreferences("ca.chancehorizon.paseo_preferences", 0)
//        val firstDashboard = paseoPrefs!!.getBoolean("prefFirstDashboard", true)
//
//        // show a welcome message to users that have launched paseo for the first time
//        if (firstDashboard == false) {
//
//            val view: View = layoutInflater.inflate(R.layout.dashboard_help_bottomsheet, null)
//
//            val dialog = BottomSheetDialog(context!!)
//            dialog.setContentView(view)
//            dialog.show()
//
//            // update shared preferences to not show first run dialog again
//            val edit: SharedPreferences.Editor = paseoPrefs.edit()
//            edit.putBoolean("prefFirstDashboard", false)
//            edit.apply()
//        }

        updateDashboard()
    }



    override fun onDestroy() {
        super.onDestroy()

        context?.unregisterReceiver(receiver);

    }



    // update the data displayed in the dashboard
    fun updateDashboard()
    {
        if (running)
        {
            var DateFormat = SimpleDateFormat("yyyyMMdd") // looks like "19891225"
            val date = DateFormat.format(Date()).toInt()
            DateFormat = SimpleDateFormat("HH")
            val hour = DateFormat.format(Date()).toInt()
            var theSteps = 0
            var daySteps = ArrayList<Array<Int>>()


            DateFormat = SimpleDateFormat("MMM dd, yyyy") // looks like "Dec 25, 1989"
            dateValue.setText("" + DateFormat.format(Date()))

            // get today's steps from the steps table in the database
            theSteps = paseoDBHelper.getDaysSteps(date)

            // display today's steps on the main screen
            stepsValue.setText(NumberFormat.getIntegerInstance().format(theSteps))
            stepsProgressBar.max = targetSteps // need to set/save target steps in preferences
            stepsProgressBar.progress = theSteps
            stepsProgressBar.secondaryProgress = targetSteps
            percentTargetValue.setText("" + Math.round(theSteps.toDouble()/targetSteps.toDouble()*100.0) + "% of target (" +
                    NumberFormat.getIntegerInstance().format(targetSteps) + ")")

            // display the "expected" steps progress (expected is the number of steps needed by this hour to achieve
            //  the daily target steps
            expectedStepsProgressBar.max = 24
            expectedStepsProgressBar.progress = hour

            // display the "expected" steps progress (expected is the number of steps needed by this hour to achieve
            //  the daily target steps
            projectedStepsProgressBar.max = targetSteps
            try {
                projectedStepsProgressBar.progress = min(theSteps * 24 / max(hour, 1), targetSteps)
            }
            catch (e: SQLiteException){

            }

            dateWeekValue.setText("Week (Mon - Sun)")

            // get this week's steps from the steps table in the database
            theSteps = paseoDBHelper.getSteps("weeks", date)

            // display today's steps on the main screen
            stepsWeekValue.setText(NumberFormat.getIntegerInstance().format(theSteps))
            stepsWeekProgressBar.max = targetWeekSteps // need to set/save target steps in preferences
            stepsWeekProgressBar.progress = theSteps
            stepsWeekProgressBar.secondaryProgress = targetWeekSteps
            percentWeekTargetValue.setText("" + Math.round(theSteps.toDouble()/targetWeekSteps.toDouble()*100.0) + "% of target (" +
                    NumberFormat.getIntegerInstance().format(targetWeekSteps) + ")")

            // display the "expected" steps progress (expected is the number of steps needed by this hour to achieve
            //  the daily target steps
            DateFormat = SimpleDateFormat("u")
            val dayOfWeek = DateFormat.format(Date()).toInt()
            expectedWeekStepsProgressBar.max = 7
            expectedWeekStepsProgressBar.progress = dayOfWeek

            // display the "expected" steps progress (expected is the number of steps needed by this hour to achieve
            //  the daily target steps
            projectedWeekStepsProgressBar.max = targetWeekSteps
            try {
                projectedWeekStepsProgressBar.progress = min(theSteps * 7 / dayOfWeek, targetWeekSteps)
            }
            catch (e: SQLiteException){
            }


            DateFormat = SimpleDateFormat("MMMM") // looks like "Dec 25, 1989"
            dateMonthValue.setText("" + DateFormat.format(Date()))

            // get this week's steps from the steps table in the database
            theSteps = paseoDBHelper.getSteps("months", date)

            // display today's steps on the main screen
            stepsMonthValue.setText(NumberFormat.getIntegerInstance().format(theSteps))
            stepsMonthProgressBar.max = targetMonthSteps // need to set/save target steps in preferences
            stepsMonthProgressBar.progress = theSteps
            stepsMonthProgressBar.secondaryProgress = targetMonthSteps
            percentMonthTargetValue.setText("" + Math.round(theSteps.toDouble()/targetMonthSteps.toDouble()*100.0) + "% of target (" +
                    NumberFormat.getIntegerInstance().format(targetMonthSteps) + ")")

            // display the "expected" steps progress (expected is the number of steps needed by this hour to achieve
            //  the daily target steps
            DateFormat = SimpleDateFormat("d")
            val dayOfMonth = DateFormat.format(Date()).toInt()
            val daysInMonth = 30 // need actual number of days in the month here ***
            expectedMonthStepsProgressBar.max = daysInMonth
            expectedMonthStepsProgressBar.progress = dayOfMonth

            // display the "expected" steps progress (expected is the number of steps needed by this hour to achieve
            //  the daily target steps
            projectedMonthStepsProgressBar.max = targetMonthSteps
            try {
                projectedMonthStepsProgressBar.progress = min(theSteps * daysInMonth / dayOfMonth, targetMonthSteps)
            }
            catch (e: SQLiteException){
            }


            DateFormat = SimpleDateFormat("YYYY") // looks like "Dec 25, 1989"
            dateYearValue.setText("" + DateFormat.format(Date()))

            // get this week's steps from the steps table in the database
            theSteps = paseoDBHelper.getSteps("years", date)

            // display today's steps on the main screen
            stepsYearValue.setText(NumberFormat.getIntegerInstance().format(theSteps))
            stepsYearProgressBar.max = targetYearSteps // need to set/save target steps in preferences
            stepsYearProgressBar.progress = theSteps
            stepsYearProgressBar.secondaryProgress = targetYearSteps
            percentYearTargetValue.setText("" + Math.round(theSteps.toDouble()/targetYearSteps.toDouble()*100.0) + "% of target (" +
                    NumberFormat.getIntegerInstance().format(targetYearSteps) + ")")

            // display the "expected" steps progress (expected is the number of steps needed by this hour to achieve
            //  the daily target steps
            DateFormat = SimpleDateFormat("D")
            val dayOfYear = DateFormat.format(Date()).toInt()
            expectedYearStepsProgressBar.max = 365
            expectedYearStepsProgressBar.progress = dayOfYear

            // display the "expected" steps progress (expected is the number of steps needed by this hour to achieve
            //  the daily target steps
            projectedYearStepsProgressBar.max = targetYearSteps
            try {
                projectedYearStepsProgressBar.progress = min(theSteps * 365 / dayOfYear, targetYearSteps)
            }
            catch (e: SQLiteException){
            }


        }
    }

    @ColorInt
    fun Context.getThemeColor(@AttrRes attribute: Int) = TypedValue().let { theme.resolveAttribute(attribute, it, true); it.data }
}


