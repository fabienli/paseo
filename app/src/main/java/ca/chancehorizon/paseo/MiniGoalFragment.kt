package ca.chancehorizon.paseo

import android.content.*
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.view.ContextThemeWrapper
import kotlinx.android.synthetic.main.fragment_mini_goal.*
import kotlinx.android.synthetic.main.fragment_mini_goal.view.*
import kotlin.math.min


class MiniGoalFragment : androidx.fragment.app.Fragment() {

    // receiver for step counting service
    private var receiver: BroadcastReceiver? = null

    private lateinit var paseoDBHelper : PaseoDBHelper

    private lateinit var contextThemeWrapper : ContextThemeWrapper

    // flag if a goal is currently in use (set by user when pressing "start" button)
    private var isGoalActive = false

    private var miniGoalSteps = 100
    private var miniGoalStartSteps = 0
    private var miniGoalEndSteps = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // point to the Paseo database that stores all the daily steps data
        paseoDBHelper = PaseoDBHelper(requireActivity())

        // set up the link between this screen and the step counting service
        configureReceiver()
    }



    // set up receiving messages from the step counter service
    private fun configureReceiver() {
        val filter = IntentFilter()
        filter.addAction("ca.chancehorizon.paseo.action")
        filter.addAction("android.intent.action.ACTION_POWER_DISCONNECTED")

        receiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                updateGoalSteps()
            }
        }

        context?.registerReceiver(receiver, filter)
    }


    override fun onDestroy() {
        super.onDestroy()

        context?.unregisterReceiver(receiver)
    }



    // re-update the screen when the user returns to it from another screen
    override fun onResume() {
        super.onResume()
        updateGoalSteps()
    }



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment

        val theTheme = requireContext().getTheme()
        contextThemeWrapper = ContextThemeWrapper(activity, theTheme)

        val view : View = inflater.inflate(R.layout.fragment_mini_goal, container, false)

        // retrieve from the saved preferences whether text to speech is available on this device
        val paseoPrefs = context?.getSharedPreferences("ca.chancehorizon.paseo_preferences", 0)
        val ttsAvailable = paseoPrefs!!.getBoolean("prefTTSAvailable", false)

        if (ttsAvailable) {

            // display the mini goals screen controls if text to speech is not available
            view.miniGoalsControls.visibility = View.VISIBLE
            view.goalStepsTitle.text = "Mini Goals"

            // respond to the user tapping on the table button
            view.startButton.setOnClickListener { startGoal() }

            // set up the number picker that the user uses (not utilizes) to set the goal steps

            var maxSteps = 10000
            var goalStepValues: MutableList<String> = ArrayList()

            var numSteps = 100

            while (numSteps <= maxSteps) {
                goalStepValues.add(numSteps.toString())
                numSteps = numSteps + 100
            }

            // set the minigoals screen items to visible
            view.miniGoalsControls.visibility = View.VISIBLE

            view.goalStepsPicker.setMinValue(1)
            view.goalStepsPicker.setMaxValue(goalStepValues.size)
            view.goalStepsPicker.setValue(10)

            val goalStepArray = goalStepValues.toTypedArray()
            view.goalStepsPicker.setDisplayedValues(goalStepArray)

            // set up the step alerts picker that the user uses (not utilizes) to set steps interval
            //  at which speech reminders are goalAlertsPicker

            // available step intervals for text to speech announcements
            goalStepValues.clear()
            goalStepValues.add("none")
            goalStepValues.add("100")
            goalStepValues.add("500")
            goalStepValues.add("1000")

            view.goalAlertsPicker.setMinValue(1)
            view.goalAlertsPicker.setMaxValue(goalStepValues.size)

            val goalAlertsArray = goalStepValues.toTypedArray()
            view.goalAlertsPicker.setDisplayedValues(goalAlertsArray)
            view.goalAlertsPicker.setWrapSelectorWheel(false)

            // get the most recently set mini goal settings to use as the default values
            // set the default mini goal steps
            miniGoalSteps = paseoPrefs!!.getInt("prefMiniGoalSteps", 20)
            view.goalStepsPicker.value = view.goalStepsPicker.displayedValues.indexOf(miniGoalSteps.toString()) + 1

            // set the default mini goal alert interval
            val miniGoalAlertInterval = paseoPrefs!!.getInt("prefMiniGoalAlertInterval", 0)
            view.goalAlertsPicker.value = view.goalAlertsPicker.displayedValues.indexOf(miniGoalAlertInterval.toString()) + 1

        }
        else
        {
            // hide the mini goals screen controls if text to speech is not available
            view.miniGoalsControls.visibility = View.GONE
            view.goalStepsTitle.text = "No Text To Speech found on this device"
        }
        return view
    }



    private fun startGoal() {

        val paseoPrefs = context?.getSharedPreferences("ca.chancehorizon.paseo_preferences", 0)

        isGoalActive = paseoPrefs!!.getBoolean("prefMiniGoalActive", false)

        // toggle running of a mini goal
        isGoalActive = !isGoalActive

        // set up the beginning of this mini goal
        if (isGoalActive) {
            // store the starting steps for this mini goal
            miniGoalStartSteps = paseoDBHelper.readLastEndSteps()

            // update shared preferences to not show first run dialog again
            val edit: SharedPreferences.Editor = paseoPrefs!!.edit()
            edit.putBoolean("prefMiniGoalActive", true)

            // note that the value saved in prefMiniGoalSteps is the index
            //  of the selected number of steps (not the number of steps)
            edit.putInt("prefMiniGoalSteps", goalStepsPicker.displayedValues[goalStepsPicker.getValue() - 1].toInt())
            edit.putInt("prefMiniGoalAlertInterval", goalAlertsPicker.displayedValues[goalAlertsPicker.getValue() - 1].toInt())
            edit.putInt("prefMiniGoalNextAlert", goalAlertsPicker.displayedValues[goalAlertsPicker.getValue() - 1].toInt())
            edit.putInt("prefMiniGoalStartSteps", miniGoalStartSteps)
            edit.putInt("prefMiniGoalStartTime", (System.currentTimeMillis() / 1000).toInt())
            edit.apply()

            goalProgressBar.max = goalStepsPicker.displayedValues[goalStepsPicker.getValue() - 1].toInt()

            goalProgressBar.secondaryProgress = goalProgressBar.max

            goalStepsPicker.isEnabled = false
            startButton.setText("Cancel")
        }
        // close this mini goal
        else{
            // update shared preferences to not show first run dialog again
            val edit: SharedPreferences.Editor = paseoPrefs!!.edit()
            edit.putBoolean("prefMiniGoalActive", false)
            edit.apply()

            goalProgressBar.secondaryProgress = 0

            goalStepsPicker.isEnabled = true

            startButton.setText("Start")
        }
    }



    // fill in the steps
    fun updateGoalSteps() {

        val paseoPrefs = context?.getSharedPreferences("ca.chancehorizon.paseo_preferences", 0)

        isGoalActive = paseoPrefs!!.getBoolean("prefMiniGoalActive", false)

        if (isGoalActive) {

            // disable the number pick for the mini goal steps (to prevent changes while goal is running
            goalStepsPicker.isEnabled = false
            startButton.setText("Cancel")

            // get the mini goal settings
            miniGoalSteps = paseoPrefs!!.getInt("prefMiniGoalSteps", 20)
            // make sure the picker is showing the current mini goal steps
            goalStepsPicker.value = goalStepsPicker.displayedValues.indexOf(miniGoalSteps.toString()) + 1

            // get the mini goal settings
            val miniGoalAlertInterval = paseoPrefs!!.getInt("prefMiniGoalAlertInterval", 0)
            // make sure the picker is showing the current mini goal steps
            goalAlertsPicker.value = goalAlertsPicker.displayedValues.indexOf(miniGoalAlertInterval.toString()) + 1

            // get the mini goal settings
            var miniGoalNextAlert = paseoPrefs!!.getInt("prefMiniGoalNextAlert", 0)

            miniGoalStartSteps = paseoPrefs!!.getInt("prefMiniGoalStartSteps", 0)
            val miniGoalStartTime = paseoPrefs!!.getInt("prefMiniGoalStartTime", 2000)

            var stepCount = 0
            // display current step count
            miniGoalEndSteps = paseoDBHelper.readLastEndSteps()
            if (goalSteps != null) {
                // display goal step count
                stepCount = miniGoalEndSteps - miniGoalStartSteps
                goalSteps.setText(stepCount.toString())
            }

            // advance the progressbar to the actual number of steps taken so far for this goal
            goalProgressBar.progress = min(stepCount, miniGoalSteps)
            goalProgressBar.max = miniGoalSteps

            // check if mini goal has been achieved
            if (stepCount >= miniGoalSteps) {
                // reset the interface
                startGoal()
                startButton.setText("Start")
            }
        }
    }
}